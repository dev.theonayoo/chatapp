
import React,{useState,useEffect} from 'react';
import {LogBox} from 'react-native';
import ChatView from './ChatView';
import Pusher from 'pusher-js/react-native';
import PusherConfig from '../pusher.json';
import Echo from 'laravel-echo';

export default function ChatClient() {

    const [messages,setMessages] = useState([]);

    LogBox.ignoreLogs(['Setting a timer for a long period of time']);

    Pusher.logToConsole = true;

    useEffect(()=>{

      const PusherClient = new Pusher(PusherConfig.key,{
        cluster: PusherConfig.cluster,
        wsHost: PusherConfig.host,
        wsPort: PusherConfig.Port,
        enabledTransports: ['ws'],
        forceTLS: false
      });

      let echo = new Echo({
        broadcaster: 'pusher',
        client: PusherClient
      });

    echo.channel('TestingChannel').listen('TestingEvent',(e) => {
        alert(e);
        setMessages(e)
      });
    },[])

  console.log('Message Incomming --->',messages);

  const handleMessage =(name, message) => {
    setMessages({action: 'message', name: name, message: message});
  }
  
  return (
      <ChatView messages={ messages } onSendMessage={ handleMessage } />
  );
}
