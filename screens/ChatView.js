import React,{createRef} from 'react';
import { StyleSheet, Text, TextInput, FlatList, KeyboardAvoidingView } from 'react-native';

export default function ChatView(props) {

  const inputText = createRef();

  const onSendMessage = (e) => {
    props.onSendMessage(e.nativeEvent.text);
    // refs.input.clear();
  }

    const renderItem = ({item})=> {
        const action = item.action;
        const name = item.name;
        if (action == 'message') {
            return <Text>{ name }: { item.message }</Text>;
        }
    }

    return (
      <KeyboardAvoidingView style={styles.container}>
        <FlatList data={ props.messages } 
                  renderItem={ renderItem } 
                  style={ styles.messages }
                />

        <TextInput autoFocus
                   clearButtonMode="always"
                   keyboardType="default" 
                   returnKeyType="done"
                   enablesReturnKeyAutomatically
                   style={ styles.input }
                   blurOnSubmit={ false }
                   onSubmitEditing={ onSendMessage }
                   ref={inputText}
                   />
      </KeyboardAvoidingView>
    );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    // paddingTop: Constants.statusBarHeight
  },
  messages: {
    alignSelf: 'stretch'
  },
  input: {
    alignSelf: 'stretch',
    backgroundColor:'#ddd'
  },
  joinPart: {
    fontStyle: 'italic'
  }
});