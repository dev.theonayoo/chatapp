import React,{useState,useEffect} from 'react';
import {LogBox,View,Text} from 'react-native';
import PusherNative from 'pusher-js/react-native';
import PusherConfig from './pusher.json';
import Echo from 'laravel-echo';


export default function App() {

  const [messages,setMessages] = useState([]);

    LogBox.ignoreLogs(['Setting a timer for a long period of time']);

    useEffect(()=>{

        PusherNative.logToConsole = true;

        const PusherClient = new PusherNative(PusherConfig.key,PusherConfig);

        const echo = new Echo({
          broadcaster: 'pusher',
          client: PusherClient
        });
        
        echo.channel('TestingChannel').listen('.TestingEvent', (e) => {
          console.log('WebSocket Data >>>',e.name);
          alert(e.name);
          setMessages(e.name);
        });

        // const channel = PusherClient.subscribe('TestingChannel');

        // channel.bind('TestingEvent',(data) => {
        //   console.log('Pusher Data >>',data);
        //   setMessages(data.name);
        // });

    },[])
       

  return (
    <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
      <Text>WebSocket Result :{messages}</Text>
    </View>
  )
}

